package com.example.myfutbooladmin.entity;

import java.util.List;

public class AppLeaguesRespons {
    private boolean success;
    private List<Leagues> leagues;

    public boolean isSuccess() {
        return success;
    }

    public List<Leagues> getLeagues() {
        return leagues;
    }
}

package com.example.myfutbooladmin.entity;

import com.google.gson.annotations.SerializedName;

public class Leagues {

    @SerializedName("id")
    private int id;
    @SerializedName("currentRound")
    private String jornada;
    @SerializedName("currentVersion")
    private String version;
    @SerializedName("appName")
    private String name;
    @SerializedName("hashcode")
    private String hashcode;
    @SerializedName("urlVideo")
    private String urlVideo;
    @SerializedName("urlAvatarPlayer")
    private String urlAvatarPlayer;
    @SerializedName("videoOnlyWifi")
    private String videoOnlyWifi;
    @SerializedName("typeVideo")
    private String typeVideo;
    @SerializedName("activeVideo")
    private String activeVideo;
    @SerializedName("activeAdsVideo")
    private String activeAdsVideo;
    @SerializedName("league_image")
    private String logoLiga;
    @SerializedName("activeUpdateApp")
    private String activeUpdateApp;

    public Leagues(int id,String name, String logoLiga,String jornada, String version) {
        this.id = id;
        this.name = name;
        this.logoLiga = logoLiga;
        this.jornada = jornada;
        this.version = version;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getJornada() {
        return jornada;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHashcode() {
        return hashcode;
    }

    public void setHashcode(String hashcode) {
        this.hashcode = hashcode;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getUrlAvatarPlayer() {
        return urlAvatarPlayer;
    }

    public void setUrlAvatarPlayer(String urlAvatarPlayer) {
        this.urlAvatarPlayer = urlAvatarPlayer;
    }

    public String getVideoOnlyWifi() {
        return videoOnlyWifi;
    }

    public void setVideoOnlyWifi(String videoOnlyWifi) {
        this.videoOnlyWifi = videoOnlyWifi;
    }

    public String getTypeVideo() {
        return typeVideo;
    }

    public void setTypeVideo(String typeVideo) {
        this.typeVideo = typeVideo;
    }

    public String getActiveVideo() {
        return activeVideo;
    }

    public void setActiveVideo(String activeVideo) {
        this.activeVideo = activeVideo;
    }

    public String getActiveAdsVideo() {
        return activeAdsVideo;
    }

    public void setActiveAdsVideo(String activeAdsVideo) {
        this.activeAdsVideo = activeAdsVideo;
    }

    public String getLogoLiga() {
        return logoLiga;
    }

    public void setLogoLiga(String logoLiga) {
        this.logoLiga = logoLiga;
    }

    public String getActiveUpdateApp() {
        return activeUpdateApp;
    }

    public void setActiveUpdateApp(String activeUpdateApp) {
        this.activeUpdateApp = activeUpdateApp;
    }
}

package com.example.myfutbooladmin.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.myfutbooladmin.entity.Leagues;
import com.example.myfutbooladmin.utils.LeagueConstants;

import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private DataBaseHelper helper;
    public SQLiteDatabase dbManager;

    public DBManager(Context context) {
        this.helper = new DataBaseHelper(context);
        this.dbManager = helper.getWritableDatabase();
    }

    public boolean insert(int id,String name,String logoLiga, String jornada, String version){
        ContentValues values = new ContentValues();
        values.put(LeagueConstants.ID,id);
        values.put(LeagueConstants.NAME_LEAGUE,name);
        values.put(LeagueConstants.LOGO_LIGA,logoLiga);
        values.put(LeagueConstants.JORNADA,jornada);
        values.put(LeagueConstants.VERSION,version);

        long result = dbManager.insert(LeagueConstants.TABLE_NAME_LIGA,null,values);
        return (result != 1);
    }

    public List<Leagues> read(){
        String [] data = new String[]{LeagueConstants.ID,LeagueConstants.NAME_LEAGUE,LeagueConstants.LOGO_LIGA,LeagueConstants.JORNADA, LeagueConstants.VERSION};
        Cursor cursor = dbManager.query(LeagueConstants.TABLE_NAME_LIGA, data,null, null,null, null,null);
        List<Leagues> league = new ArrayList<>();
        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex(LeagueConstants.ID));
            String name = cursor.getString(cursor.getColumnIndex(LeagueConstants.NAME_LEAGUE));
            String logoLiga = cursor.getString(cursor.getColumnIndex(LeagueConstants.LOGO_LIGA));
            String jornada = cursor.getString(cursor.getColumnIndex(LeagueConstants.JORNADA));
            String version = cursor.getString(cursor.getColumnIndex(LeagueConstants.VERSION));

            Leagues iLeague = new Leagues(id,name,logoLiga,jornada, version);
            league.add(iLeague);
        }

        return league;

    }

}

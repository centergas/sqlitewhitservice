package com.example.myfutbooladmin.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.myfutbooladmin.utils.LeagueConstants;

public class
DataBaseHelper extends SQLiteOpenHelper {


    public DataBaseHelper(Context context) {
        super(context, LeagueConstants.DB_NAME, null,LeagueConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String adminBase = "create table "+LeagueConstants.TABLE_NAME_LIGA+" ("+LeagueConstants.ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                " "+LeagueConstants.NAME_LEAGUE+" TEXT, "+LeagueConstants.LOGO_LIGA+" TEXT,"+LeagueConstants.JORNADA+" TEXT, "+LeagueConstants.VERSION+" TEXT)";
        db.execSQL(adminBase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String adminBase = "DROP TABLE EXIST" +LeagueConstants.TABLE_NAME_LIGA;
        db.execSQL(adminBase);
        onCreate(db);
    }
}

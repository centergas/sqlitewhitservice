package com.example.myfutbooladmin.utils;

public class LeagueConstants {
    //url api
    public static final String BASE_URL = "http://somosdefutbol.net/api/admin/";

   //constantes base de datos Sqlite
    //nombre base de datos
    public static final String DB_NAME = "DatabaseHelper.db";
    //nombre tabla base da datos
    public static final String TABLE_NAME_LIGA= "leagues";
    //propiedades tabla
    public static final String NAME_LEAGUE = "name";
    public static final String LOGO_LIGA = "logoLiga";
    public static final String JORNADA = "jornada";
    public static final String VERSION = "version";
    public static final String ID = "id";
    public static final int DB_VERSION = 1;

}

package com.example.myfutbooladmin.api;

import android.app.Notification;

import com.example.myfutbooladmin.utils.LeagueConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {

    public ApiLeague getApi(){

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS
                )
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LeagueConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(ApiLeague.class);
    }
}

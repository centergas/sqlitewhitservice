package com.example.myfutbooladmin.api;

import com.example.myfutbooladmin.entity.AppLeaguesRespons;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiLeague {

    @GET("leagues")
    Call<AppLeaguesRespons> leaguesRequest();

}

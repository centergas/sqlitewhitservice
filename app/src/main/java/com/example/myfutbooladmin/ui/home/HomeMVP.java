package com.example.myfutbooladmin.ui.home;

import com.example.myfutbooladmin.entity.AppLeaguesRespons;
import com.example.myfutbooladmin.entity.Leagues;

import java.util.List;
import retrofit2.Call;

public interface HomeMVP {

    interface Model{
        Call<AppLeaguesRespons> getLeagues();
        void saveLeagues(List<Leagues>leaguesList);
        List<Leagues> selectLeagues();
    }

    interface Presenter{
        void setView(View view);
        void getLeagues();
    }

    interface View{
        void showLeagues(List<Leagues> leaguesRespons);
        void showMessage(String string);
    }
}

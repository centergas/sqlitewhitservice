package com.example.myfutbooladmin.ui.home;

import android.content.Context;
import com.example.myfutbooladmin.entity.AppLeaguesRespons;
import com.example.myfutbooladmin.entity.Leagues;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterHome implements HomeMVP.Presenter{
    private HomeMVP.Model model;
    private HomeMVP.View view;
    private Context context;


    public PresenterHome(Context context) {
        this.context = context;
        this.model = new ModelHome(context);
    }
    @Override
    public void setView(HomeMVP.View view) {
        this.view = view;
    }

    @Override
    public void getLeagues() {
        model.getLeagues().enqueue(new Callback<AppLeaguesRespons>() {
            @Override
            public void onResponse(Call<AppLeaguesRespons> call, Response<AppLeaguesRespons> response) {
                if (response.isSuccessful()){
                    List<Leagues> leagues = new ArrayList<>();
                    leagues = response.body().getLeagues();

                    if (leagues != null && leagues.size() > 0){
                        model.saveLeagues(leagues);
                        view.showLeagues(model.selectLeagues());
                    }else {
                        view.showMessage("la lista viene vacia");
                    }


                }else {
                    view.showMessage("Error al consultar datos");
                }
            }
            @Override
            public void onFailure(Call<AppLeaguesRespons> call, Throwable t) {
                view.showMessage("Error de servidor");
            }
        });
    }
}

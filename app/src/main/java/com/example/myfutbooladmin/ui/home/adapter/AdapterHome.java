package com.example.myfutbooladmin.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfutbooladmin.entity.Leagues;

import java.util.List;

public class AdapterHome extends RecyclerView.Adapter<HolderHome> {

    private List<Leagues> responsList;

    public AdapterHome(List<Leagues> responsList) {
        this.responsList = responsList;
    }

    @NonNull
    @Override
    public HolderHome onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HolderHome(com.example.myfutbooladmin.databinding.ItemLeaguesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderHome holder, int position) {
        holder.addItem(responsList.get(position));

    }

    @Override
    public int getItemCount() {
        return responsList != null ? responsList.size() : 0;
    }
}

package com.example.myfutbooladmin.ui.home;

import android.content.Context;
import com.example.myfutbooladmin.api.ApiLeague;
import com.example.myfutbooladmin.api.Client;
import com.example.myfutbooladmin.entity.AppLeaguesRespons;
import com.example.myfutbooladmin.entity.Leagues;
import com.example.myfutbooladmin.sqlite.DBManager;
import java.util.List;
import retrofit2.Call;

public class ModelHome implements HomeMVP.Model{
    private ApiLeague apiLeague;
    private DBManager dbManager;

    public ModelHome(Context context) {
        Client client = new Client();
        this.dbManager = new DBManager(context);
        this.apiLeague = client.getApi();
    }


    @Override
    public Call<AppLeaguesRespons> getLeagues() {
        return apiLeague.leaguesRequest();
    }

    @Override
    public void saveLeagues(List<Leagues> leaguesList) {
        for (Leagues item : leaguesList){
            dbManager.insert(item.getId(),item.getName(),item.getLogoLiga(),item.getJornada(),item.getVersion());
        }
    }

    @Override
    public List<Leagues> selectLeagues() {
        return dbManager.read();
    }

}

package com.example.myfutbooladmin.ui.home.adapter;

import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.myfutbooladmin.databinding.ItemLeaguesBinding;
import com.example.myfutbooladmin.entity.Leagues;

public class HolderHome extends RecyclerView.ViewHolder {
    private ItemLeaguesBinding binding;

    public HolderHome(ItemLeaguesBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void addItem(Leagues respons){
        Glide.with(itemView.getContext()).load(respons.getLogoLiga()).circleCrop().into(binding.ivLiga);
        binding.tvNameLeague.setText(respons.getName());
        binding.tvJornada.setText(respons.getJornada());
        binding.tvVersion.setText(respons.getVersion());
        Glide.with(itemView.getContext()).load(respons.getLogoLiga()).into(binding.ivStatus);
    }
}

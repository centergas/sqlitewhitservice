package com.example.myfutbooladmin.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.widget.Toast;

import com.example.myfutbooladmin.databinding.HomeLeaguesActivityBinding;
import com.example.myfutbooladmin.entity.Leagues;
import com.example.myfutbooladmin.ui.home.adapter.AdapterHome;

import java.util.List;

public class HomeLeaguesActivity extends AppCompatActivity implements HomeMVP.View{
    private HomeLeaguesActivityBinding binding;
    private HomeMVP.Presenter presenter;
    private AdapterHome adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = HomeLeaguesActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        presenter = new PresenterHome(this);
        presenter.setView(this);
        presenter.getLeagues();
    }

    @Override
    public void showLeagues(List<Leagues> leaguesRespons) {
        adapter = new AdapterHome(leaguesRespons);
        binding.rvLeagfues.setLayoutManager(new LinearLayoutManager(this));
        binding.rvLeagfues.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String string) {
    }

}